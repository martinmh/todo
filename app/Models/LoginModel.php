<?php namespace app\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
    protected $table = 'user';

    protected $allowedFields = ['username','password', 'firstname', 'lastname'];

    public function check($username,$password) {
        $this->where('username', $username);
        $query = $this->get();
        //print $this->getLastQuery();
        $row = $query->getRow();
        if ($row) { // Check if SQL returned a row.
            if (password_verify($password,$row->password)) { // Verify password
                return $row;
            }
        }
        return null;
    }
}